%% \documentclass[preprint,numbers,10pt]{sigplanconf}
\documentclass[numbers,10pt]{sigplanconf}

\usepackage{makeidx}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{indentfirst}
\usepackage{verbatim}
\usepackage{amsmath, amssymb}
\usepackage{graphicx}
\usepackage{color}
\usepackage{url}
%\usepackage[belowskip=-15pt,aboveskip=0pt]{caption}

\definecolor{light-gray}{gray}{0.90}
\newcommand{\graybox}[1]{\colorbox{light-gray}{\texttt{#1}}}

\lstdefinelanguage{ocaml}{
keywords={let, begin, end, in, match, type, and, fun, function, try, with, class, 
object, method, of, rec, repeat, until, while, not, do, done, as, val, inherit, 
new, module, sig, deriving, datatype, struct, if, then, else, ostap, open, generic, virtual},
sensitive=true,
basicstyle=\small,
commentstyle=\small\itshape\ttfamily,
keywordstyle=\ttfamily\underbar,
identifierstyle=\ttfamily,
basewidth={0.5em,0.5em},
columns=fixed,
fontadjust=true,
literate={->}{{$\to$}}1,
morecomment=[s]{(*}{*)}
}

\lstset{
basicstyle=\small,
identifierstyle=\ttfamily,
keywordstyle=\bfseries,
commentstyle=\scriptsize\rmfamily,
basewidth={0.5em,0.5em},
fontadjust=true,
escapechar=~,
language=ocaml
}

\sloppy
\newcommand{\cc}{\mbox{ :: }}
\newcommand{\opt}[1]{\mbox{[ }#1\mbox{ ] }}
\newcommand{\etc}{\mbox{ }\ldots}
\newcommand{\term}[1]{\mbox{\texttt{#1}}}
\newcommand{\nt}[1]{$#1$}
\newcommand{\meta}[1]{\textsc{#1}}
\newcommand{\spc}{\mbox{ }}
\newcommand{\alt}{\mbox{ | }}
\newcommand{\trans}[3]{{#1}\vdash{#2}\to{#3}}
\newcommand{\cd}[1]{\texttt{#1}}
\newcommand{\kw}[1]{\texttt{\textbf{#1}}}
\newcommand{\comm}[1]{\textit{\small{#1}}}
\newcommand{\tv}[1]{\mbox{$'{#1}$}}
\newcommand{\inmath}[1]{\mbox{\lstinline{#1}}}
\newcommand{\goal}{\mathfrak G}

\newcommand{\app}[1]{\textcolor{red}{(Anton: #1)}}
\newcommand{\evg}[1]{\textcolor{blue}{(Evgenii: #1)}}

\newcommand{\readInst }[3][\ExclReadType]{#2 \;:=_{\excl{#1}}\;[#3]}
\newcommand{\readPlnInst}[2]{#1 \;:=\;[#2]}
\newcommand{\fenceInst}[1]{\dmb \; #1}
\newcommand{\dmbSY}{\fenceInst{\SY}}
\newcommand{\dmbLD}{\fenceInst{\LD}}

\newcommand\ifGoto{\textsf{if}-\textsf{goto}}
\newcommand{\ifGotoInst}[2]{\textsf{if} \; #1 \; \textsf{goto} \; #2}
\newcommand{\writeInst}[2]{[#1]\;:=\;#2}
\newcommand{\writeMoInst}[3]{[#1]_{#2}\;:=\;#3}
\newcommand{\writeExclInst}[3]{\textup{\sf atomic-write}(#1, [#2], #3)}
\newcommand{\assignInst}[2]{#1\;:=\;#2}
\newcommand{\repeatInst}[1]{\textsf{repeat} \; #1 \; \textsf{end}}
\newcommand{\returnInst}[1]{\textsf{return} \; #1}

\begin{document}

%\mainmatter

\title{Relational Interpretation of Concurrency\\
{\small (Extended Abstract)}}

\authorinfo{Evgenii Moiseenko}
{JetBrains Research, St. Petersburg University}
{e.moiseenko@2012.spbu.ru\\
Postal address: 70/2, Botanicheskaya street, 198504, St. Petersburg, Russia\\
Scientific advisor: Anton Podkopaev\\
ACM student member number: 1923513\\
Category: graduate
}

%\author{}
%\institute{St. Petersburg State University, St. Petersburg, Russia\\
%\email{lozov.peter@gmail.com}\\
%Postal address: 70/3, Botanicheskaya street, 198504, St. Petersburg, Russia\\
%Scientific advisor: Dmitri Boulytchev\\
%ACM student member number: 4970108\\
%Category: graduate}

\maketitle

In connection with the spread of multi-core processors, the modern software heavily utilizes the shared state concurrency.
However, a developer, who wants to use concurrency in his program, has to be very cautious about it and write a lot of
auxiliary and sophisticated code to properly synchronize parallel computations.

Consider the following program\footnote{In all examples we assume that locations are initialized with $0$.}:
\[
  \begin{array}{l || l}
    \writeInst{x}{1}; & \repeatInst{f}; \\
    \writeInst{f}{1}; & \readPlnInst{a}{x}{}; \\
  \end{array}
\]
Here the left thread writes a value $1$ to a shared memory location $x$ and then wants to 
pass this value to the right thread. 
In order to inform the thread that the value is stored to $x$ and is available to read,
the left thread sets a shared flag $f$. 
The right thread waits in a loop until it reads $1$ from $f$. After that the thread reads from $x$.
Note that even in this short and simple example 
the half of the code (the manipulations on the flag $f$) 
is auxiliary, and its only purpose is to synchronize threads.
What if we could write the essential part of code only and generate necessary synchronization automatically?

Our work tries to positively answer the question with help of \emph{relational programming}~\cite{friedman2005reasoned}.
Relational programming is a paradigm, which is very close to functional programming, but the main abstraction is
\emph{relations} rather than functions. In functional programming, one may provide arguments to a function
and ask for a result. In relational programming, one may provide some parameters to a relation and \emph{query}
it for others. For example, the relation, that binds the positive integer number with its square, 
can be used to find the square-root of a number. 
It is sufficient to supply for this relation the second argument
and ask it to compute the first one.    
Partially defined arguments can be given to a relation as well.
In this case a query to the relation will determine argument's omitted parts.
Thus a single relational specification can be used to solve a number of related problems.
For example, one may use a relational typechecker to perform type inference or
to find a type inhabitant.

Another notable example is a relational interpreter for an abstract machine.
The interpreter might be represented as an evaluation relation, that
binds an initial state of the machine and its terminal state.
On the one hand, one can query such an interpreter to find all terminal states that are reachable from a given initial state.
On the other hand, the interpreter may generate possibly infinite stream of initial states that when being 
executed reduce to the provided terminal state. 
A more interesting use case of a relation interpreter is to generate a complete program
from a partially written. Thus one may write a program leaving some \emph{holes} in it
and set constraints on terminal states of program's execution.
Having this, a relational interpreter would be able to process the partial program with the given constraints and
fill the holes with terms such that a resulting program would have the desired behavior.
We argue that this allows to use a relation interpreter to generate synchronization code in concurrent programs.

To evaluate the idea, we wrote a relational interpreter for a concurrent language using miniKanren~\cite{byrd2009relational}---a DSL for relational programming. 
More specifically, we used OCanren~\cite{kosarev2016typed} 
that is a strongly-typed version of miniKanren embedded into OCaml language.
However, the original implementation of OCanren lacked the support of \emph{tabling}.
Tabling is an extension of \emph{memoization}---a common technique 
in the field of functional programming, that saves the results
of function application and avoids their recalculation in the further calls.
Tabling generalizes this idea to relations. 
Tabling is particularly useful for fixpoint computations (such as our evaluation relation)
because it avoids unnecessary duplicate computations and
helps many otherwise divergent programs to complete.
Without tabling many of our test programs either diverged or 
took a significant amount of time to complete.
We have extended the OCanren with the tabling 
that is based on the Scheme implementation described in~\cite{byrd2009relational}.

%% The syntax of our concurrent language
%% comprises local variable assignments, loads and stores to shared locations, repeat and if-then-else constructions,
%% and parallel composition. 

We applied our interpreter to the following partially defined program, which
is the message passing example considered earlier but without synchronization:
\[\begin{array}{l || l}
  \writeInst{x}{1};      & \textsf{[?]};  \\
  \textsf{[?]};           & \readPlnInst{a}{x}; \\
\end{array}\]
With the number of constraints described below, our interpreter generates expected terms successfully.  
For the first hole it proposes the atomic write to $f$ the unit.
For the second hole it generates repeat loop that tests the 
result of an atomic read from $f$. 
As an another option, it proposes to wait in a loop for the result of atomic read from $x$, 
but this solution is less desirable, since the first thread may write any value to $x$ including the zero value. 
It is important to give to the interpreter a proper specification of the desired behavior of partial program.
In this example, we say that any possible execution of the partial program should lead to a state where the variable $a$ contains value $1$.
In order to narrow the search space and speed-up the search, we also
constraint the form of holes to expressions and statements on boolean variables. 

Consider an another example of partial program:
\[\begin{array}{l || l}
  \textsf{[?]};          & \textsf{[?]};         \\
  \textsf{if [?] then}   & \textsf{if [?] then}  \\
  \;\; \writeInst{z}{1}; & \;\; \writeInst{z}{2}; \\
\end{array}\]
The desired behavior in this example is to guarantee that only one thread will 
perform the write instruction. 
Thus, it can be seen as a simplified version of mutual exclusion problem. 
We ask our interpreter to fill the holes and it gives us the following result:
\[\begin{array}{l || l}
  \writeInst{y}{1};          & \writeInst{x}{1};        \\
  \textsf{if x == 0 then}   & \textsf{if y == 0 then}   \\
  \;\; \writeInst{z}{1}; & \;\; \writeInst{z}{2};       \\
\end{array}\]
One may observe that this code is similar to simplified Dekker's algorithm.
The form of the holes was restricted to operations on booleans as in previous example.

So far we have implicitly assumed that all instructions 
in single thread are executed in the same order as they appear in program code
and that as far as the thread observes a write to some location, it also observes all preceded writes.
Generally, these assumptions holds only in \emph{sequentially-consistent}~\cite{lamport1979make} setting.
However, a lot of concurrent systems have more complicated behaviors
\cite{batty2011mathematizing,manson2005java,flur2016modelling,alglave2014herding}. 
Such systems are usually referred as \emph{weak memory models}. 
Obviously, interpreting a program under weak memory is harder than under sequentially-consistent
since the first allows a greater number of behaviors. Hence the state space may increase dramatically.

We extend our interpreter to support memory models, which are weaker than sequential-consistency.
Our current implementation can handle release-writes and acquire-reads. 
Also we added to our interpreter non-atomic operations 
which are read and writes from shared memory locations 
that do not synchronize the concurrent threads that perform these operations.
Incorrect use of non-atomic operations may lead to data-races. 
Our interpreter is capable to detect such misuses and report them.

Wickerson et al.~\cite{wickerson2017automaticallyb} also addresses a problem of program synthesis in the 
settings of weak memory with a relational language Alloy~\cite{jackson2012software}.
However, that work concerns mostly axiomatic semantics,
while our relational interpreter exploits recently proposed operational semantics~\cite{podkopaev2016operational,lahav2016taming,kang2017promising}. 
Another difference is that Wickerson et al. uses SAT solvers, but our solution
is based on two features of miniKanren: syntactic unification and interleaving search.

Back to the message passing example,
one can get the desired behavior under memory model 
weaker than sequential consistent:
\[\begin{array}{l || l}
  \writeMoInst{x}{na}{1};  & \repeatInst{f_{acq}}; \\
  \writeMoInst{f}{rel}{1}; & \readPlnInst{a}{x};          \\
\end{array}\]
As one can see, it is sufficient to use non-atomic operations on the location $x$
and mark the write to flag $f$ as a release-write and 
read from $f$ as an acquire-read.
We asked our relational interpreter to generate 
the synchronization code in the message passing example again,
but now under the release-acquire memory model.
It successfully guessed that in this weaker model
the write to $f$ should be the release write and
read from $f$ should be the acquire read.

The summarizing statistics that contains number of unification,
which is a basic primitive in miniKanren, and run time in seconds for all examples 
are presented below.

\[\begin{tabular}{l*{6}{c}r}
Program              & \#holes & \#unifications & runtime (sec) \\
\hline
MP\_SC               & 2 & 60515   & 0.17  \\
MP\_RelAcq           & 2 & 159015  & 0.92  \\
Dekker\_SC           & 4 & 2034801 & 71.49 \\
\end{tabular}\]

As a direction for future work we plan to support
other operational weak memory models. 
We are going to add \emph{relaxed} read and writes
and handle them via promise machine which was described in~\cite{kang2017promising}.
Another direction of our work would be
to scale our interpreter for synthesis the holes
in bigger and more complicated programs.
It is also desirable to allow the interpreter  
make guesses about form of holes without a hints from a 
programmer.

\bibliographystyle{abbrv}
\bibliography{main}

\end{document}
